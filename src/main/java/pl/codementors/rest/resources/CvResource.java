package pl.codementors.rest.resources;

import pl.codementors.rest.CvDataStore;
import pl.codementors.rest.model.CV;
import pl.codementors.rest.model.Education;
import pl.codementors.rest.model.Info;
import pl.codementors.rest.model.WorkExperience;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Arrays;

@Path("cv")
public class CvResource {

    @Inject
    private CvDataStore store;

    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CV getCv() {
        return store.getCv();
    }

    @Path("/")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void addWorkExp(WorkExperience workExperience) {
        store.addWorkExp(workExperience);
    }

}
